<?php

   session_start();
    if ($_SESSION['user']) {
        header('Location: profile.php');
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="conteiner mt-5">
        <div class="row justify-content-center ">
            <div class="mx-auto" style="width: 200px;">
                <form class="form-group" action="vendor/signup.php" method="post" enctype="multipart/form-data">
                <label>Почта</label>
                <input type="email" name="email" placeholder="Введите email"> <br>
                <label>Логин</label>
                <input type="text" name="login" placeholder="Введите свой логин"> <br>
                <label>Пароль</label>
                <input type="password" name="password" placeholder="Введите пароль"> <br>
                <label>Подтверждение пароля</label>
                <input type="password" name="password_confirm" placeholder="Подтвердите пароль"> <br>
                <label>ФИО</label>
                <input type="text" name="full_name" placeholder="Введите свое полное имя">
                <button type="submit">Зарегистрировать</button>
                <p>
                    У вас уже есть аккаунт? - <a href="/">авторизируйтесь</a>!
                </p>

                <?php
                if ($_SESSION['message']) {
                    echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
                }
                unset($_SESSION['message']);
                ?>
                </form>
            </div>
        </div>
    </div>
    </body>
</html>
<?php
session_start();
if (!$_SESSION['user']) {
    header('Location: /');
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Авторизация и регистрация</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>

<!-- Профиль -->



<form action="../update.php" method="post">
    <div class="container">
        <div class="row">
            <div class="col-1">
                <a>ФИО</a>
            </div>
            <div class="col">
                <input type="hidden" name="id" value="<?= $_SESSION['user']['id'] ?>">
                <input required type="text" name="full_name" value="<?= $_SESSION['user']['full_name'] ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <a>Почта</a>
            </div>
            <div class="col">
                <input required type="text" name="email" value="<?= $_SESSION['user']['email'] ?>">
            </div>
        </div>
        <div class="row">
            <div class="col-1">
                <a>Пароль</a>
            </div>
            <div class="col">
                <input type="password" name="password">
                <input id="submit" type="submit" value="Сохранить">
                <a href="vendor/logout.php" class="logout">Выход</a>
            </div>
        </div>
    </div>
</form>



</body>
</html>